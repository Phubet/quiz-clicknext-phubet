import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/about',
    name: 'About',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/no2',
    name: 'No1',
    component: () =>
      import(/* webpackChunkName: "about" */ '../components/No2.vue')
  },
  {
    path: '/no4',
    name: 'No4',
    component: () =>
      import(/* webpackChunkName: "about" */ '../components/No4.vue')
  },
  {
    path: '/no6',
    name: 'No6',
    component: () =>
      import(/* webpackChunkName: "about" */ '../components/No6.vue')
  },
  {
    path: '/no7',
    name: 'No1',
    component: () =>
      import(/* webpackChunkName: "about" */ '../components/No.7.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
